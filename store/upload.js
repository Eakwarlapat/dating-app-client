export const state = () => ({
  users: null,
})

export const getters = {
  users: (state) => state.users,
}

export const mutations = {
  SET_USERS(state, users) {
    state.users = users
  }
}

export const actions = {
  async users({ commit }) {
    let { data } = await this.$axios.get('users')
    return data;
  },
}
