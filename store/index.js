export const state = () => ({
  users: null,
})

export const getters = {
  users: (state) => state.users,
}

export const mutations = {
  SET_USERS(state, users) {
    state.users = users
  }
}

export const actions = {
  async register({ commit }, payload) {
    let { data } = await this.$axios.post('/auth/register', payload)
    return data
  },
  async login({ commit }, payload) {
    let { data } = await this.$axios.post('login', payload)
    return data
  },
  async logout({ commit }, payload) {
    let { data } = await this.$axios.post('logout', payload)
    return data
  },
  async users({ commit }) {
    let { data } = await this.$axios.get('users')
    commit('SET_USERS', data)
  },
}
